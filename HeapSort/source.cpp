#include <iostream>
#include <stdlib.h>
#include <time.h> 
using namespace std;

void heapify(int array[], int i, int rozmiar) {
    int najwiekszy;
    int lewy_i = 2 * i + 1;
    int prawy_i = 2 * i + 2;

    if (lewy_i < rozmiar && array[lewy_i] > array[i])
        najwiekszy = lewy_i;
    else
        najwiekszy = i;

    if (prawy_i < rozmiar && array[prawy_i] > array[najwiekszy])
        najwiekszy = prawy_i;

    if (najwiekszy != i) {
        swap(array[i], array[najwiekszy]);
        heapify(array, najwiekszy, rozmiar);
    }
}

void build_heap(int array[], int rozmiar) {
    for (int i = rozmiar / 2 - 1; i >= 0; i--)
        heapify(array, i, rozmiar);
}

void heapSort(int array[], int rozmiar) {
    build_heap(array, rozmiar);
    for (int i = rozmiar - 1; i >= 0; i--) {
        swap(array[0], array[i]);
        rozmiar = rozmiar - 1;
        heapify(array, 0, rozmiar);
    }
}

void print(int array[], int n) {
    cout << endl;
    for (int i = 0; i < n; i++) {
        cout << array[i] << " ";
    }
}

int main() {
    int n = 100;
    int array[100];
    srand(time(NULL));
    for (int i = 0; i < n; i++) {
        array[i] = rand() % 1000 + 1;;
    }
    print(array, n); //nieposortowana tablica
    heapSort(array, n);
    print(array, n); //posortowana tablica
}