#include <iostream>
#include <stdlib.h>
#include <time.h>  

using namespace std;

void insertionSort(int array[], int n) {
    int i = 0, elem = 0;
    for (int j = 1; j < n; j++) {
        elem = array[j];
        i = j - 1;
        while (i >= 0 && array[i] > elem) {
            array[i + 1] = array[i];
            i = i - 1;
        }
        array[i + 1] = elem;
    }
}

void print(int array[], int n) {
    cout << endl;
    for (int i = 0; i < n; i++) {
        cout << array[i] << " ";
    }
}

int main() {
    int n = 100;
    int array[100];
    srand(time(NULL));
    for (int i = 0; i < n; i++) {
        array[i] = rand() % 1000 + 1;;
    }
    print(array, n); //nieposortowana tablica
    insertionSort(array, n);
    print(array, n); //posortowana tablica
}