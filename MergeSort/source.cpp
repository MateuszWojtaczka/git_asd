#include <iostream>
#include <stdlib.h>
#include <time.h> 

using namespace std;

void Merge(int array[], int p, int r, int q) {
    int i, j, k, left_size, right_size;
    left_size = q - p + 1;
    right_size = r - q;
    int left_array[left_size], right_array[right_size];
    for (i = 0; i < left_size; i++) {
        left_array[i] = array[p + i];
    }
    for (j = 0; j < right_size; j++) {
        right_array[j] = array[q + 1 + j];
    }
    i = 0; j = 0; k = p;
    while (i < left_size && j < right_size) {
        if (left_array[i] <= right_array[j]) {
            array[k] = left_array[i];
            i++;
        }
        else {
            array[k] = right_array[j];
            j++;
        }
        k++;
    }
    while (i < left_size) {
        array[k] = left_array[i];
        i++;
        k++;
    }
    while (j < right_size) {
        array[k] = right_array[j];
        j++;
        k++;
    }
}

void MergeSort(int array[], int p, int r) {
    if (p < r) {
        int q = (p + r) / 2;

        MergeSort(array, p, q);
        MergeSort(array, q + 1, r);
        Merge(array, p, r, q);
    }
}

void print(int array[], int n) {
    cout << endl;
    for (int i = 0; i < n; i++) {
        cout << array[i] << " ";
    }
}

int main() {
    int n = 100;
    int array[100];
    srand(time(NULL));
    for (int i = 0; i < n; i++) {
        array[i] = rand() % 1000 + 1;;
    }
    print(array, n); //nieposortowana tablica
    MergeSort(array, 0, n);
    print(array, n); //posortowana tablica
}